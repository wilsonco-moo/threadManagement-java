/*
 * Java Thread-management library
 * Copyright (C) October 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package threadManagement;

/**
 *
 * @author wilson
 */
public abstract class RunObject implements Runnable {
    public final String name;
    public volatile boolean run = true; // Whether to continue to run this object. If this is set to false before run is started, run will be cancelled.
    public volatile boolean completed = false; // Whether the thread has started running.
    public RunObject(String name) {
        this.name = name;
    }
    /**
     * This is run if the runObject failed to execute for whichever reason - any generic Exception,
     * This method can and should be overridden.
     * @param e 
     */
    public void failed(Exception e) {
        e.printStackTrace();
    }
    /**
     * This is run if the runObject failed to execute due to an OutOfMemoryError
     * This method can and should be overridden.
     * @param e 
     */
    public void failed(OutOfMemoryError e) {
        e.printStackTrace();
    }
    
    public final void runProperly() {
        if (run)
            try {
                completed = true;
                run();
            } catch (OutOfMemoryError err) {
                failed(err);
            } catch (Exception err) {
                failed(err);
            }
    }
}
