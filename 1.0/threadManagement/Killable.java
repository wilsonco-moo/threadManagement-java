/*
 * Java Thread-management library
 * Copyright (C) October 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package threadManagement;

/**
 * Ending stuff became complicated, so this interface allows any class to work in a similar way,
 * it provides a simple method for killing the threads associated with the thing,
 * this does not have to kill the object immediately, just has to do it within a reasonable time,
 * 
 * any killable objects contained within this killable object, must also have their kill methods
 * run when the kill method is run, this way the entire system can be stopped with one method,
 * 
 * A method waitForEnd must also be implemented, whereby it waits for the end, then returns.
 * 
 * @author wilson
 */
public interface Killable {
    public void kill();
    public void waitForEnd();
    public boolean isRunning();
}
