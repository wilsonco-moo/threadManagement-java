/*
 * Java Thread-management library
 * Copyright (C) October 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package threadManagement.queue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

/**
 *
 * @author wilson
 */
public class ThreadActionPanel extends JPanel {
    public final JLabel[] threadLabels;
    public final int numberOfThreads;
    private final GridLayout layout;
    public final static int height = 32;
    public final static Color backgroundForRunningThreads = new Color(0.4f,0.9f,0.4f,1f);
    ThreadActionPanel(MultiThreadQueue queue, int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
        setLayout(new GridLayout(1,numberOfThreads));
        threadLabels = new JLabel[numberOfThreads];
        layout = new GridLayout(1,numberOfThreads);
        layout.setHgap(4);
        setLayout(layout);
        int i = 0; JLabel j; while (i < numberOfThreads) {
            j = new JLabel("-",SwingConstants.CENTER);
            j.setBorder(BorderFactory.createLoweredBevelBorder());
            add(j);
            threadLabels[i] = j; i++;
        }
    }
    
    @Override public Dimension getPreferredSize() {
        return new Dimension(getWidth(),height);
    }
    @Override public Dimension getMinimumSize() {
        return new Dimension(1,height);
    }
    @Override public Dimension getMaximumSize() {
        return new Dimension(MultiThreadQueue.big,height);
    }
    
    void showLabel(int thread, String text) {
        JLabel j = threadLabels[thread]; j.setText(text);
        j.setOpaque(true); j.setBackground(backgroundForRunningThreads);
        SwingUtilities.invokeLater(new Runnable() {
            @Override public void run() {
                repaint();
            }
        });
    }
    
    void hideLabel(int thread) {
        JLabel j = threadLabels[thread]; j.setText("-");
        j.setOpaque(false); j.setBackground(null);
        SwingUtilities.invokeLater(new Runnable() {
            @Override public void run() {
                repaint();
            }
        });
    }
}
