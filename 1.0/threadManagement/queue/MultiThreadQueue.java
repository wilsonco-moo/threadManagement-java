/*
 * Java Thread-management library
 * Copyright (C) October 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package threadManagement.queue;

import threadManagement.RunObject;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import threadManagement.Killable;


/**
 * Modified: 30th October 2017 - synchronisation fix.
 * @author wilson
 */
public class MultiThreadQueue implements Killable {
    private final MultiThreadQueueThread[] threads; // The array of threads
    public final int numberOfThreads;
    
    /**
     * Synchronisation with this is required to access any of the queues.
     */
    private final Object queueLockObject = new Object();
    
    
    
    private volatile int nextThreadToUse = 0;
    public final static int big = (int)(((long)Math.pow(2d,31d))-1l);
    public volatile boolean autoLog = false;
    
    private final ThreadActionPanel threadActionPanel;
    
    private final Queue<RunObject> priorityQueue = new LinkedList<RunObject>();
    public final String name;
    
    public MultiThreadQueue(int numberOfThreads, String name) {
        this.name = name;
        this.numberOfThreads = numberOfThreads;
        threadActionPanel = new ThreadActionPanel(this,numberOfThreads);
        threads = new MultiThreadQueueThread[numberOfThreads]; // Create thread array
        int i = 0;
        while (i < numberOfThreads) { // Loop through
            threads[i] = new MultiThreadQueueThread(i); // Create threads
            threads[i].start(); // Start them
            i++;
        }
    }
    
    public ThreadActionPanel getActionPanel() {
        return threadActionPanel;
    }
    
    
    /**
     * Sets all threads to end when next operation is finished, also notifies them so they end immediately if waiting around.
     */
    @Override public void kill() {
        int i = 0;
        while (i < numberOfThreads) { // Loop through
            threads[i].continueThread = false;
            synchronized(threads[i].threadLock) {
                threads[i].threadLock.notify();
            }
            i++;
        }
        notKilled = false; synchronized(killLock) { killLock.notify(); }
    }
    
    private final Object killLock = new Object(); private volatile boolean notKilled = false;
    @Override public void waitForEnd() {
        while(notKilled)
            try { synchronized(killLock) { killLock.wait(); } } catch(InterruptedException ie) {}
    }
    @Override public boolean isRunning() { return notKilled; }
    
    
    /**
     * The class controlling each thread queue.
     */
    private class MultiThreadQueueThread extends Thread {
        final Queue<RunObject> queue = new LinkedList<RunObject>(); // Queue of objects to run
        public final Object threadLock = new Object(); // Lock for thread wait/notify
        public boolean continueThread = true; // Whether to continue thread
        public final int threadId;
        
        private MultiThreadQueueThread(int threadId) {
            super(name+": "+threadId);
            this.threadId = threadId;
        }
        
        @Override public void run() {
            boolean isEmpty; RunObject e = null;
            while(continueThread) {
                
                /*
                Note: Deciding whether to wait, and actually waiting MUST BE DONE
                in the same synchronization block, as otherwise a run object can be
                added to the queue between the two happening. This caused the notify
                to not happen sometimes.
                */
                synchronized(threadLock) { // Sync on the lock for THIS THREAD.
                    synchronized(queueLockObject) { // Then sync on the central queue lock
                        if (priorityQueue.isEmpty()) { // If nothing in priority queue
                            isEmpty = queue.isEmpty(); // Get whether queue is empty
                            if (!isEmpty) e = queue.remove(); // If not empty, get next from queue
                        } else { // Otherwise
                            isEmpty = false; // Set empty to false
                            e = priorityQueue.remove(); // Get from priority queue
                        }
                    }
                    
                     // Then wait outside of the main queue lock, to avoid deadlock.
                    if (isEmpty) { // If queue is empty
                        threadActionPanel.hideLabel(threadId);
                        try { threadLock.wait(); } catch (InterruptedException ex) {} // Wait
                    }
                }

                if (!isEmpty) {
                    if (e.run) { // If e should run
                        threadActionPanel.showLabel(threadId,e.name);
                        if (autoLog) System.out.println("STARTING "+e.name);
                        try {
                            e.run(); // Run/print
                        } catch (OutOfMemoryError err) {
                            e.failed(err);
                        } catch (Exception err) {
                            e.failed(err);
                        }
                        if (autoLog) System.out.println("DONE "+e.name);
                    } else {
                        if (autoLog) System.out.println("CANCELLED "+e.name); // Or print cancelled
                    }
                }
            }
            System.out.println("Multi-thread queue: "+getName()+", ended.");
        }
    }
    
    /**
     * Gets the thread id which has the shortest queue.
     * @return 
     */
    public int getMostEmptyThread() {
        synchronized(queueLockObject) {
            int mostEmpty = big, mostEmptyId = 0, i = 0, e, sz;
            while(i < numberOfThreads) {
                e = (i+nextThreadToUse)%numberOfThreads;
                sz = threads[e].queue.size();
                if (sz < mostEmpty) {
                    mostEmpty = sz;
                    mostEmptyId = e;
                }
            }
            nextThread();
            return mostEmptyId;
        }
    }
    
    /**
     * Adds the specified RunObjects to the same thread, so one is completed after another, in order - returning the thread's id.
     * @param r
     * @return Returns the thread ID it is put on.
     */
    public int addToThreadQueue(RunObject ... r) {
        // NOTE: DO NOT NEST SYNCS HERE, AS IT
        // WOULD CAUSE DEADLOCKING.
        int threadId;
        synchronized(queueLockObject) {
            threads[nextThreadToUse].queue.addAll(Arrays.asList(r));
            threadId = nextThread();
        }
        synchronized(threads[threadId].threadLock) {
            threads[threadId].threadLock.notify();
        }
        return threadId;
    }
    
    
    /**
     * Adds the specified RunObjects to the priority queue - will be run as soon as possible
     * @param r
     */
    public void bargeToFrontOfQueue(RunObject ... r) {
        synchronized(queueLockObject) {
            priorityQueue.addAll(Arrays.asList(r)); // Add to priority queue
        }
        for (MultiThreadQueueThread t : threads) { // Notify all threads
            synchronized(t.threadLock) {
                t.threadLock.notify();
            }
        }
    }
    
    /**
     * Adds the specified RunObjects to the different threads.
     * @param r
     */
    public void addRunObjectsToDifferentThreadQueues(RunObject ... r) {
        for (RunObject e : r) {
            int threadId;
            synchronized(queueLockObject) {
                threads[nextThreadToUse].queue.add(e);
                threadId = nextThread();
            }
            synchronized(threads[threadId].threadLock) {
                threads[threadId].threadLock.notify();
            }
        }
    }
    
    /**
     * Adds the specified RunObjects to a specific thread, so one is completed after another, in order.
     * @param threadQueueId
     * @param r
     */
    public void addRunObjectsToSpecificThreadQueue(int threadQueueId, RunObject ... r) {
        synchronized(queueLockObject) {
            threads[threadQueueId].queue.addAll(Arrays.asList(r));
        }
        synchronized(threads[threadQueueId].threadLock) {
            threads[threadQueueId].threadLock.notify();
        }
    }



    private int nextThread() {
        int e = nextThreadToUse;
        nextThreadToUse = (nextThreadToUse+1)%numberOfThreads;
        return e;
    }
}
