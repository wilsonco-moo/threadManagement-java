/*
 * Java Thread-management library
 * Copyright (C) October 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package threadManagement.schedule;

import threadManagement.RunObject;

/**
 *
 * @author wilson
 */
public class ThreadScheduleObject {
    public int numberOfIntervalsBetweenRuns;
    int timeIntervalUpTo;
    public final RunObject runObject;
    ThreadScheduleObject(RunObject runObject, int numberOfIntervalsBetweenRuns, int offset) {
        this.numberOfIntervalsBetweenRuns = numberOfIntervalsBetweenRuns;
        timeIntervalUpTo = offset;
        this.runObject = runObject;
    }
    
    void incrementAndRun(boolean autoLog) {
        timeIntervalUpTo++;
        if (timeIntervalUpTo >= numberOfIntervalsBetweenRuns) {
            if (runObject.run) {
                if (autoLog) System.out.println("SCHEDULED RUN: "+runObject.name);
                try {
                    runObject.run(); // Run/print
                } catch (OutOfMemoryError err) {
                    runObject.failed(err);
                } catch (Exception err) {
                    runObject.failed(err);
                }
            }
            timeIntervalUpTo = 0;
        }
    }
}
