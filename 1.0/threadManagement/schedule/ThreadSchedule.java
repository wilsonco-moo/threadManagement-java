/*
 * Java Thread-management library
 * Copyright (C) October 2017, Daniel Wilson
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package threadManagement.schedule;

import java.util.LinkedList;
import threadManagement.Killable;
import threadManagement.RunObject;


/**
 *
 * @author wilson
 */
public class ThreadSchedule implements Killable {
    private long timeUpTo; // The time the thread schedule is up to
    private final ThreadScheduleThread thread;
    public volatile int interval;
    private volatile boolean continueThread = true;
    public boolean autoLog = false;
    
    private final Object threadScheduleObjectsListLock = new Object();
    private LinkedList<ThreadScheduleObject> threadScheduleObjects = new LinkedList<ThreadScheduleObject>();
    
    int i = 0;
    
    public final String name;
    
    

    @Override public void kill() {
        continueThread = false;
    }
    private final Object killLock = new Object();
    @Override public void waitForEnd() {
        while(continueThread)
            try { synchronized(killLock) { killLock.wait(); } } catch(InterruptedException ie) {}
    }
    @Override public boolean isRunning() { return continueThread; }
    
    
    private class ThreadScheduleThread extends Thread {
        private ThreadScheduleThread() {
            super(name);
        }
        @Override public void run() {
            while(continueThread) {
                synchronized(threadScheduleObjectsListLock) {
                    for (ThreadScheduleObject t : threadScheduleObjects) {
                        t.incrementAndRun(autoLog);
                    }
                }
                waitForNextInterval();
            }
            synchronized(killLock) { killLock.notify(); }
            System.out.println("Thread schedule: "+name+", ended.");
        }
        
        /**
         * Waits until next interval is due, waits less time if behind etc.
         */
        public void waitForNextInterval() {
            timeUpTo += interval; // Add interval onto timeUpTo
            long sleepTime = timeUpTo-System.currentTimeMillis();
            if (sleepTime > 0) {
                try {
                    Thread.sleep(sleepTime);
                } catch (InterruptedException ex) {}
            }
        }
    }
    
    public ThreadSchedule(int interval, String name) {
        this.name = name;
        this.interval = interval;
        thread = new ThreadScheduleThread();
        timeUpTo = System.currentTimeMillis();
        thread.start();
    }
    
    public ThreadScheduleObject addToSchedule(RunObject runObject, int numberOfIntervalsBetweenRuns) {
        synchronized(threadScheduleObjectsListLock) {
            ThreadScheduleObject t = new ThreadScheduleObject(runObject, numberOfIntervalsBetweenRuns, 0);
            threadScheduleObjects.add(t);
            return t;
        }
    }
    
    public ThreadScheduleObject addToSchedule(RunObject runObject, int numberOfIntervalsBetweenRuns, int offset) {
        synchronized(threadScheduleObjectsListLock) {
            ThreadScheduleObject t = new ThreadScheduleObject(runObject, numberOfIntervalsBetweenRuns, offset);
            threadScheduleObjects.add(t);
            return t;
        }
    }
    
    public void removeFromSchedule(ThreadScheduleObject threadScheduleObject) {
        synchronized(threadScheduleObjectsListLock) {
            threadScheduleObjects.remove(threadScheduleObject);
        }
    }
}
