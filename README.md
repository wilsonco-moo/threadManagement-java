# Java thread management library

## Overview

This is a Java library, designed to aid multithreaded applications, which I designed between February 2017 and October 2017.
The [simplenetwork library](https://gitlab.com/wilsonco-moo/simplenetwork-java) depends on this for the `Killable` interface.

The library features three main things:
 * `MultiThreadQueue` is a simple thread pool, designed to queue tasks to run on many different threads.
 * `ThreadSchedule` allows tasks to be scheduled to run at regular intervals.
 * `Killable` is an interface which defines a standard set of behaviour for objects which start threads.

In this library, tasks are defined using the `RunObject` abstract class. `RunObject` allows additional functionality over the
standard `Runnable` interface, such as being able to cancel the task in a `MultiThreadQueue` before it starts, by setting the
`run` field to false.

## A simple thread pool: `MultiThreadQueue`

This class acts as a simple thread pool. When created, the user must specify how many threads it should use, and a name. The name
is used to name the threads that are created, for debugging purposes. As with all `Killable` classes, the methods `kill()` and
`waitForEnd()` can be called at any point in time.

To add to the `MultiThreadQueue`, simply create a `RunObject`, and call the thread queue's `addToThreadQueue(RunObject ... r)` method.
There are other methods for adding to the thread queue, all of which work in subtlety different ways. See the javadoc comments in
`MultiThreadQueue` for more information.

Here is an example program that uses `MultiThreadQueue`. This program does nothing, but shows how to create a `MultiThreadQueue`
and add tasks to it.

```java
package simplenetworkdemo;
import threadManagement.RunObject;
import threadManagement.queue.MultiThreadQueue;

public class ThreadQueueDemo {
    public static void main(String[] args) {
        
        // Create a MultiThreadQueue, using 4 threads.
        MultiThreadQueue queue = new MultiThreadQueue(4, "Demo queue");
        
        // Add a task to the queue
        queue.addToThreadQueue(new RunObject("Demo run object") {
            @Override public void run() {
                // ... Do something
            }
        });
        
        // ... later in the program
        
        // We can end the thread queue using the kill method().
        queue.kill();
    }
}
```

## A system for running tasks periodically: `ThreadSchedule`

The `ThreadSchedule` class allows tasks to be run periodically, in a convenient way. When created, the user must specify an interval
in milliseconds, and a name. The name is used for debug purposes, as in `MultiThreadQueue`. The interval specifies how long the schedule
thread should sleep between performing tasks. The class takes into account how long each task takes when sleeping, to ensure that
tasks are run at as close to their specified interval as possible.

Here is an example program that uses `ThreadSchedule`. This program does nothing, but shows how to create a `ThreadSchedule`
and add tasks to it.

```java
package simplenetworkdemo;
import threadManagement.RunObject;
import threadManagement.schedule.ThreadSchedule;

public class ThreadScheduleDemo {
    public static void main(String[] args) {
        
        // Create a schedule with an interval of 100 milliseconds.
        ThreadSchedule schedule = new ThreadSchedule(100, "Demo thread schedule");
        
        // Add a task to the thread schedule.
        // This task will be run every 5 intervals of the thread schedule, which
        // is 5 * 100 milliseconds = 500 milliseconds.
        // NOTE: The first run of the task will be as soon as the thread schedule
        // does an interval.
        schedule.addToSchedule(new RunObject("Demo run object 1") {
            @Override public void run() {
                // ... Do something. This will be run every 0.5 seconds.
            }
        }, 5);
        
        // Add another task to the thread schedule.
        // This task will be run every 10 intervals of the thread schedule,
        // which is 10 * 100 milliseconds = 1000 milliseconds.
        // NOTE: For this task we specify an offset of 2 intervals. This means
        // the first run of the task will be after 2 intervals = 200 milliseconds.
        schedule.addToSchedule(new RunObject("Demo run object 2") {
            @Override public void run() {
                // ... Do something. This will be run every 1 second.
            }
        }, 10, 2);
        
        // ... later in the program
        
        // We can end the thread schedule using the kill method().
        schedule.kill();
    }
}
```
